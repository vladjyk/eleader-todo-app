package biz.eleader.todoapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import biz.eleader.todoapp.data.db.entity.Todo
import biz.eleader.todoapp.data.db.entity.TodoDao

@Database(entities = [Todo::class], version = 6, exportSchema = false)
abstract class ApplicationDatabase : RoomDatabase() {
    abstract fun todoDao(): TodoDao

    companion object {
        @Volatile
        private var instance: ApplicationDatabase? = null

        operator fun invoke(context: Context): ApplicationDatabase = synchronized(this) {
            return instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context): ApplicationDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                ApplicationDatabase::class.java,
                "todo_app.db"
            ).fallbackToDestructiveMigration().build()
        }

    }
}