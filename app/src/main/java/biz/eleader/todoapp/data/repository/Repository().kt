package biz.eleader.todoapp.data.repository

import androidx.lifecycle.LiveData
import biz.eleader.todoapp.data.db.ApplicationDatabase
import biz.eleader.todoapp.data.db.entity.Todo

class Repository(db: ApplicationDatabase) {
    private val todoDao = db.todoDao()

    fun insert(todo: Todo){
        todoDao.insert(todo)
    }

    fun deleted(todo: Todo){
        todoDao.delete(todo)
    }

    fun deleteAllTodo(){
        todoDao.deleteAll()
    }

    fun getAllTodo(): LiveData<List<Todo>>{
        return todoDao.getAllLive()
    }

    fun isTodoAlreadyExist(todo: Todo): Boolean{
        return todoDao.getByParams(todo.name, todo.executionDateStr, todo.type).isNotEmpty()
    }
}