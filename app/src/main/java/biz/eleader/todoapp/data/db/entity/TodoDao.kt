package biz.eleader.todoapp.data.db.entity

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TodoDao{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(todo: Todo)

    @Query("SELECT * FROM todo_table ORDER BY id DESC")
    fun getAllLive(): LiveData<List<Todo>>

    @Delete
    fun delete(todo: Todo)

    @Query("DELETE FROM todo_table")
    fun deleteAll()

    @Query("SELECT * FROM todo_table WHERE name == :name AND executionDateStr == :executionDateStr AND type == :type")
    fun getByParams(name: String, executionDateStr: String, type: Int): List<Todo>
}