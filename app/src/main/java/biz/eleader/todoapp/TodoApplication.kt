package biz.eleader.todoapp

import android.app.Application
import biz.eleader.todoapp.data.db.ApplicationDatabase
import biz.eleader.todoapp.data.repository.Repository
import biz.eleader.todoapp.ui.childs.vm.AddEditTodoActivityVMFactory
import biz.eleader.todoapp.ui.main.vm.MainActivityVMFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class TodoApplication : Application(), KodeinAware{
    override val kodein = Kodein.lazy {
        import(androidXModule(this@TodoApplication))

        bind() from singleton { ApplicationDatabase(this@TodoApplication) }
        bind() from singleton { Repository(instance()) }

        bind() from provider { MainActivityVMFactory(instance()) }
        bind() from provider { AddEditTodoActivityVMFactory(instance()) }
    }


}