package biz.eleader.todoapp.ui.main.adapters

import android.content.Context
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import biz.eleader.todoapp.R
import biz.eleader.todoapp.data.db.entity.Todo
import biz.eleader.todoapp.databinding.ItemTodoBinding

private const val ITEM_LAYOUT = R.layout.item_todo

class TodoAdapter(private val listener: ItemInteractionListener) :
    ListAdapter<Todo, TodoAdapter.ViewHolder>(DIFF_CALLBACK) {

    private lateinit var context: Context
    private val todoTypes: List<String> by lazy {
        context.resources.getStringArray(R.array.todo_types).toList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (!::context.isInitialized)
            context = parent.context

        val inflater = LayoutInflater.from(context)
        val binding = ItemTodoBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bindData(item)
    }

    inner class ViewHolder(private val binding: ItemTodoBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnCreateContextMenuListener {

        private var todo: Todo? = null

        init {
            binding.root.setOnCreateContextMenuListener(this)
        }

        fun bindData(todo: Todo) {
            this.todo = todo

            binding.todo = todo
            binding.listener = listener
            binding.todoTypes = todoTypes
            binding.executePendingBindings()
        }

        override fun onCreateContextMenu(
            menu: ContextMenu?,
            v: View?,
            menuInfo: ContextMenu.ContextMenuInfo?
        ) {
            fun createItems() {
                menu?.add(R.string.main_activity_item_ctx_menu_delete)?.setOnMenuItemClickListener {
                    todo?.let {
                        listener.onDelete(it)
                    }
                    true
                }
            }

            createItems()
        }
    }


    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Todo>() {
            override fun areContentsTheSame(
                oldItem: Todo,
                newItem: Todo
            ): Boolean {
                return oldItem.name == newItem.name &&
                        oldItem.executionDateStr == newItem.executionDateStr &&
                        oldItem.type == newItem.type

            }

            override fun areItemsTheSame(oldItem: Todo, newItem: Todo) =
                oldItem.id == newItem.id
        }
    }

    interface ItemInteractionListener {
        fun onClick(item: Todo)
        fun onDelete(item: Todo)
    }
}