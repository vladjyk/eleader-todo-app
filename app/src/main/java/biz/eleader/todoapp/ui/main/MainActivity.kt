package biz.eleader.todoapp.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import biz.eleader.todoapp.R
import biz.eleader.todoapp.data.db.entity.Todo
import biz.eleader.todoapp.databinding.ActivityMainBinding
import biz.eleader.todoapp.extend.ui.MyActivity
import biz.eleader.todoapp.ui.childs.AddEditTodoActivity
import biz.eleader.todoapp.ui.childs.EXTRA_TODO
import biz.eleader.todoapp.ui.main.adapters.TodoAdapter
import biz.eleader.todoapp.ui.main.vm.MainActivityVM
import biz.eleader.todoapp.ui.main.vm.MainActivityVMFactory
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance

private const val LAYOUT = R.layout.activity_main

class MainActivity : MyActivity(), TodoAdapter.ItemInteractionListener {
    override val titleRes = R.string.main_activity_title
    override val menuRes = R.menu.main_activity_menu
    override val exitBtnIconRes: Int? = null

    private val viewModelFactory by instance<MainActivityVMFactory>()
    private lateinit var viewModel: MainActivityVM

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        fun initBinding() {
            binding = DataBindingUtil.setContentView(this, LAYOUT)
        }

        fun initViewModel() {
            viewModel = ViewModelProvider(this, viewModelFactory).get(MainActivityVM::class.java)
        }

        fun bindData() {
            binding.activity = this
            binding.listener = this
            viewModel.getAllTodo().observe(this, Observer {
                binding.todoList = it
            })
        }

        super.onCreate(savedInstanceState)
        initBinding()
        initViewModel()
        bindData()
    }


    fun onClickFab() {
        showAddEditActivity()
    }

    override fun onClick(item: Todo) {
        showAddEditActivity(item)
    }

    private fun showAddEditActivity(todo: Todo? = null) {
        Intent(this, AddEditTodoActivity::class.java).apply {
            todo?.let {
                putExtra(EXTRA_TODO, it)
            }
            startActivity(this)
        }
    }


    override fun onDelete(item: Todo) {
        viewModel.delete(item)

        Snackbar.make(coordinatorLayout, R.string.main_activity_delete_msg, Snackbar.LENGTH_LONG)
            .setAction(R.string.main_activity_delete_abort) {
                viewModel.insert(item)
            }.setAnimationMode(Snackbar.ANIMATION_MODE_SLIDE).show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.action_delete_all_items)
           onDeleteAll()

        return super.onOptionsItemSelected(item)
    }

    private fun onDeleteAll(){
        viewModel.deleteAllTodo()
        Snackbar.make(coordinatorLayout, R.string.main_activity_delete_all_msg, Snackbar.LENGTH_LONG).show()
    }
}
