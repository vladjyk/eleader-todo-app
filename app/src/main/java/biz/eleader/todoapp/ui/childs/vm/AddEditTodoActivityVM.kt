package biz.eleader.todoapp.ui.childs.vm

import androidx.lifecycle.ViewModel
import biz.eleader.todoapp.data.db.entity.Todo
import biz.eleader.todoapp.data.repository.Repository

class AddEditTodoActivityVM(private val repository: Repository): ViewModel() {

    fun insert(todo: Todo){
        repository.insert(todo)
    }

    fun isSameTodoAlreadyExist(todo: Todo): Boolean{
        return repository.isTodoAlreadyExist(todo)
    }
}