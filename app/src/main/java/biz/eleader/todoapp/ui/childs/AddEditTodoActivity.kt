package biz.eleader.todoapp.ui.childs

import android.os.Bundle
import android.view.MenuItem
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProvider
import biz.eleader.todoapp.R
import biz.eleader.todoapp.data.db.entity.Todo
import biz.eleader.todoapp.extend.ui.MyActivity
import biz.eleader.todoapp.ui.childs.dialog.DatePickerDialog
import biz.eleader.todoapp.ui.childs.dialog.ItemAlreadyExistDialog
import biz.eleader.todoapp.ui.childs.vm.AddEditTodoActivityVM
import biz.eleader.todoapp.ui.childs.vm.AddEditTodoActivityVMFactory
import biz.eleader.todoapp.util.DateUtil
import kotlinx.android.synthetic.main.activity_add_edit_todo.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

const val EXTRA_TODO = "biz.eleader.todoapp.ui.childs.todo"
private const val LAYOUT = R.layout.activity_add_edit_todo

class AddEditTodoActivity : MyActivity(), DatePickerDialog.OnDateIsSetListener {
    private val isCreatingNewItem: Boolean
        get() {
            return !intent.hasExtra(EXTRA_TODO)
        }

    override val titleRes by lazy {
        if (isCreatingNewItem)
            R.string.add_edit_activity_new_item_title
        else R.string.add_edit_activity_edit_item_title
    }
    override val exitBtnIconRes = R.drawable.ic_close_white_24dp
    override val menuRes: Int = R.menu.add_edit_activity_menu

    private val viewModelFactory by instance<AddEditTodoActivityVMFactory>()
    private lateinit var viewModel: AddEditTodoActivityVM

    private var todo: Todo? = null

    private lateinit var datePickerDialog: DatePickerDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        fun getExtraData() {
            intent.apply {
                if (hasExtra(EXTRA_TODO))
                    todo = getParcelableExtra(EXTRA_TODO)

            }
        }

        fun initViewModel() {
            viewModel =
                ViewModelProvider(this, viewModelFactory).get(AddEditTodoActivityVM::class.java)
        }

        fun requestFocusOnNameInput() {
            nameTIET.requestFocus()
        }

        fun initDatePickerDialog() {
            datePickerDialog = DatePickerDialog.getInstance(this, this)
        }

        fun initTodoName() {
            todo?.let {
                nameTIET.setText(it.name)
            }
        }

        fun initExecutionDate() {
            executionDateTV.text = todo?.executionDateStr ?: DateUtil.getToday()
        }

        fun initTypeSpinner() {
            val todoTypes = resources.getStringArray(R.array.todo_types)
            val adapter =
                ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, todoTypes)
            typeSpinner.apply {
                this.adapter = adapter
                this.selection = todo?.type ?: 0
            }
        }

        fun setOnClickListeners() {
            executionDateTV.setOnClickListener {
                val initialDateStr = executionDateTV.text.toString()
                val initialDate = DateUtil.parseStringDateToDate(initialDateStr)
                datePickerDialog.show(initialDate)
            }
        }

        super.onCreate(savedInstanceState)
        setContentView(LAYOUT)
        getExtraData()
        initViewModel()
        requestFocusOnNameInput()
        initDatePickerDialog()
        initTodoName()
        initExecutionDate()
        initTypeSpinner()
        setOnClickListeners()
    }

    override fun onDateIsPicked(dateStr: String, dateMillis: Long) {
        executionDateTV.text = dateStr
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val dateStr = executionDateTV.text.toString()
        outState.putString("dateStr", dateStr)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        val dateStr = savedInstanceState.getString("dateStr")
        executionDateTV.text = dateStr
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_save)
            onClickSave()

        return super.onOptionsItemSelected(item)
    }

    private fun onClickSave() {
        fun isValidInput(): Boolean {
            return nameTIET.text.toString().isNotEmpty()
        }

        fun showInputError() {
            val msg = getString(R.string.add_edit_activity_todo_name_empty_error)
            nameTIET.error = msg
        }

        fun initTodoObjectByInputs() {
            if (todo == null)
                todo = Todo()

            todo?.let {
                it.name = nameTIET.text.toString()
                it.executionDateStr = executionDateTV.text.toString()
                it.type = typeSpinner.selection
            }
        }


        fun showItemAlreadyExistDialog() {
            ItemAlreadyExistDialog.getInstance(this)
                .show{
                    GlobalScope.launch {
                        viewModel.insert(todo!!)
                    }

                    toast(R.string.add_edit_activity_add_successful_msg)
                    finish()
                }
        }

        fun tryToSave() {
            GlobalScope.launch {
               val isExist = viewModel.isSameTodoAlreadyExist(todo!!)

                if (isExist && isCreatingNewItem)
                    showItemAlreadyExistDialog()

                else {
                    val msg = if(isCreatingNewItem) R.string.add_edit_activity_add_successful_msg
                    else R.string.add_edit_activity_edit_successful_msg

                    viewModel.insert(todo!!)
                    toast(msg)
                    finish()
                }
            }
        }


        if (isValidInput()) {
            initTodoObjectByInputs()
            tryToSave()
        } else {
            showInputError()
        }
    }

}
