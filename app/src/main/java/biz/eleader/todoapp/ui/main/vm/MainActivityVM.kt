package biz.eleader.todoapp.ui.main.vm

import androidx.lifecycle.ViewModel
import biz.eleader.todoapp.data.db.entity.Todo
import biz.eleader.todoapp.data.repository.Repository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivityVM (private val repository: Repository): ViewModel(){
    fun insert(todo: Todo) = GlobalScope.launch{
        repository.insert(todo)
    }

    fun delete(todo: Todo) = GlobalScope.launch{
        repository.deleted(todo)
    }

    fun deleteAllTodo() = GlobalScope.launch {
        repository.deleteAllTodo()
    }

    fun getAllTodo() = repository.getAllTodo()
}
