package biz.eleader.todoapp.ui.childs.dialog

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import biz.eleader.todoapp.R

private const val TAG = "ItemAlreadyExistDialog"

private const val TITLE = R.string.dialog_item_already_exist_title
private const val MSG = R.string.dialog_item_already_exist_msg
private const val POSITIVE_BTN = R.string.dialog_item_already_exist_positive_btn_title
private const val NEGATIVE_BTN = R.string.dialog_item_already_exist_negative_btn_title

class ItemAlreadyExistDialog: DialogFragment() {
    private var fm: FragmentManager? = null

    private lateinit var dialog: AlertDialog

    private var onSaveAnyway: ( ()-> Unit )? = null

    fun show(onSaveAnyway : ()-> Unit) = fm?.let {
        fun isAlreadyVisible(): Boolean{
            it.fragments.forEach {visibleFragment ->
               if(visibleFragment.tag == TAG)
                return true
            }
            return false
        }

        if (!isAlreadyVisible()) {
            show(it, TAG)
            this.onSaveAnyway = onSaveAnyway
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        fun buildDialog(){
            val onClickPositive: (DialogInterface, Int) -> Unit = { _, _ ->
                onSaveAnyway?.invoke()
                dismiss()
            }

            val onClickNegative: (DialogInterface, Int) -> Unit = { _, _ ->
                dismiss()
            }

            dialog = AlertDialog.Builder(context!!)
                .setTitle(TITLE)
                .setMessage(MSG)
                .setPositiveButton(POSITIVE_BTN, onClickPositive)
                .setNegativeButton(NEGATIVE_BTN, onClickNegative)
                .create()
        }

        super.onCreate(savedInstanceState)
        buildDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = dialog

    companion object{
        fun getInstance(context: Context) = ItemAlreadyExistDialog().apply {
            fm = (context as AppCompatActivity).supportFragmentManager
        }
    }
}