package biz.eleader.todoapp.ui.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import biz.eleader.todoapp.data.db.entity.Todo

class BindingAdapter {
    companion object {
        @JvmStatic
        @BindingAdapter("android:todoList", "android:itemInteractionListener")
        fun setTodoList(
            recyclerView: RecyclerView,
            todoList: List<Todo>?,
            listener: TodoAdapter.ItemInteractionListener?
        ) {
            if (todoList == null || listener == null) return

            val layoutManager = recyclerView.layoutManager
            val context = recyclerView.context

            if (layoutManager == null) {
                recyclerView.layoutManager = LinearLayoutManager(context)
            }

            if (recyclerView.adapter == null) {
                TodoAdapter(listener).apply {
                    recyclerView.adapter = this
                    submitList(todoList)
                    notifyDataSetChanged()
                }
            } else {
                (recyclerView.adapter as TodoAdapter).apply {
                    submitList(todoList)
                    notifyDataSetChanged()
                }
            }
        }
    }
}