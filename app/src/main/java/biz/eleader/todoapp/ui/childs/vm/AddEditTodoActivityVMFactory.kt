package biz.eleader.todoapp.ui.childs.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import biz.eleader.todoapp.data.repository.Repository

class AddEditTodoActivityVMFactory (private val repository: Repository): ViewModelProvider.NewInstanceFactory(){
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddEditTodoActivityVM(repository) as T
    }
}