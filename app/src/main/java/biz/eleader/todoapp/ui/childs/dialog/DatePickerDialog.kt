package biz.eleader.todoapp.ui.childs.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import biz.eleader.todoapp.R
import biz.eleader.todoapp.util.DateUtil
import kotlinx.android.synthetic.main.dialog_date_picker.view.*
import java.lang.Exception
import java.util.*

private const val TAG = "DatePickerDialog"
private const val DIALOG_LAYOUT = R.layout.dialog_date_picker

class DatePickerDialog: DialogFragment(){
    private lateinit var ctx: Context
    private lateinit var dialogView: View
    private lateinit var dialog: AlertDialog

    private lateinit var fm: FragmentManager

    private var initialDate: Calendar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        fun initDialog(){
            val inflater = LayoutInflater.from(context)

            dialogView = inflater.inflate(DIALOG_LAYOUT, null, false)
            initialDate?.let {
                val day = it.get(Calendar.DAY_OF_MONTH)
                val month = it.get(Calendar.MONTH)
                val year = it.get(Calendar.YEAR)
                dialogView.datePicker.updateDate(year, month, day)
            }
            dialog = AlertDialog.Builder(context!!).setView(dialogView).create()
        }

        fun setMinDate(){
            val now = Calendar.getInstance().timeInMillis
            dialogView.datePicker.minDate = now
        }

        fun setClickListeners(){
            dialogView.apply {
                applyBtn.setOnClickListener {
                    Calendar.getInstance().apply {
                        set(Calendar.DAY_OF_MONTH, datePicker.dayOfMonth)
                        set(Calendar.MONTH, datePicker.month)
                        set(Calendar.YEAR, datePicker.year)

                        val dateStr = DateUtil.dateToStr(this)
                        listener.onDateIsPicked(dateStr, timeInMillis)
                    }
                    dismiss()
                }

                cancelBtn.setOnClickListener {
                    this@DatePickerDialog.dismiss()
                }
            }
        }

        super.onCreate(savedInstanceState)
        initDialog()
        setMinDate()
        setClickListeners()
    }

    fun show(initialDate: Calendar?){
        fun isAlreadyVisible(): Boolean{
            fm.fragments.forEach {
                if (it.tag == TAG)
                    return true
            }

            return false
        }

        if (!isAlreadyVisible()){
            show(fm, TAG)
            this.initialDate = initialDate
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = dialog

    companion object{
        private lateinit var listener: OnDateIsSetListener

        fun getInstance(context: Context, listener: OnDateIsSetListener) = DatePickerDialog().apply {
            fm = (context as AppCompatActivity).supportFragmentManager
            ctx = context
            this@Companion.listener = listener
        }
    }

    interface OnDateIsSetListener{
        fun onDateIsPicked(dateStr: String, dateMillis: Long)
    }
}