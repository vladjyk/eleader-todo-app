package biz.eleader.todoapp.extend.ui

import android.view.Menu
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import biz.eleader.todoapp.R
import kotlinx.android.synthetic.main.app_bar_layout.*
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

abstract class MyActivity : AppCompatActivity(), KodeinAware{
    override val kodein by closestKodein()

    abstract val titleRes: Int
    abstract val menuRes: Int?
    abstract val exitBtnIconRes: Int?

    override fun onStart() {
        fun initActionBar(){
            toolbar?.let {
                setSupportActionBar(it)
                if (exitBtnIconRes != null){
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    supportActionBar?.setHomeAsUpIndicator(exitBtnIconRes!!)
                }
            }
        }

        fun setTitle(){
            title = getString(titleRes)
        }

        super.onStart()
        initActionBar()
        setTitle()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuRes?.let {
            menuInflater.inflate(it, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    protected fun toast(msg: String) = GlobalScope.launch{
        launch (Main){
            Toast.makeText(this@MyActivity, msg, Toast.LENGTH_SHORT).show()
        }
    }

    protected fun toast(@StringRes msgRes: Int) = GlobalScope.launch{
        launch (Main) {
            Toast.makeText(this@MyActivity, msgRes, Toast.LENGTH_SHORT).show()
        }
    }


}