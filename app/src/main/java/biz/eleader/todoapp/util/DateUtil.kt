package biz.eleader.todoapp.util

import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    companion object {
        fun getToday(): String {
            val date = Calendar.getInstance()
            return dateToStr(date)
        }

        fun dateToStr(date: Calendar): String {
            val dayOfMonth = date.get(Calendar.DAY_OF_MONTH)
            val month = date.get(Calendar.MONTH) + 1
            val year = date.get(Calendar.YEAR)

            return buildString {
                if (dayOfMonth < 10)
                    append("0$dayOfMonth")
                else append(dayOfMonth)

                append(".")

                if (month < 10)
                    append("0$month")
                else append(month)

                append(".")
                append(year)
            }
        }

        fun parseStringDateToDate(dateStr: String): Calendar?{
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            val date = simpleDateFormat.parse(dateStr) ?: return null

            return Calendar.getInstance().apply {
                timeInMillis = date.time
            }
        }
    }
}