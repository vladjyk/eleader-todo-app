package biz.eleader.todoapp.util

import org.junit.Assert.*
import org.junit.Test

class DateUtilTest{
    @Test
    fun nowStr() {
        val today = DateUtil.getToday()
        assertEquals("11.02.2020", today)
    }

    @Test
    fun dateStrToDate() {
        val dateStr = "18.01.2020"
        val date = DateUtil.parseStringDateToDate(dateStr)

        val result = DateUtil.dateToStr(date!!)
        assertEquals(dateStr, result)
    }
}